from django.urls import path
from . import views

urlpatterns = [
    # default page
    path('', views.index),
    # web-route to a create a new show
    path('new', views.new),
    # form-action to create a new show
    path('create', views.create),
    # web-route to edit a single show
    path('<int:show_id>/edit', views.edit),
    # form-action to edit a single show
    path('<int:show_id>/update', views.update),
    # web-route to a single show
    path('<int:show_id>', views.show),
    # trigger to delete a single show
    path('<int:show_id>/delete', views.delete)
]
